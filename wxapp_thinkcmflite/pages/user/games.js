
var api = require('../../utils/api.js')
var app = getApp()



Page({

    /**
     * 页面的初始数据
     */
    data: {
        status:0,
        url:api.CURL,
        list:[1,2]
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        
    },
    onShow(){
        this.getmytackbytype();
    },

    getstatus(ret){
        console.log(ret);
        var i=ret.currentTarget.dataset.i;

        this.setData({
            status:i
        })


        if(i==0){
            this.getmytackbytype();
        }else if(i==1){
            this.getmytackbytype1();
        }else if(i==2){
            this.getmytackbytype2();
        }else if(i==3){
            this.getmytackbytype3();
        }
        
    },
    getmytackbytype2(){

        var that=this;
        var status=this.data.status;

        that.setData({
            list: []
        });

        api.get({
            url: 'wxapp/game/getmytackbytype2',
            data: {status:status},
            success: data => {
                console.log(data.data);

                that.setData({
                    list: data.data
                });
            }
        });


    },
    getmytackbytype3(){

        var that=this;
        var status=this.data.status;

        that.setData({
            list: []
        });

        api.get({
            url: 'wxapp/game/getmytackbytype3',
            data: {status:status},
            success: data => {
                console.log(data.data);

                that.setData({
                    list: data.data
                });
            }
        });


    },

    getmytackbytype1(){

        var that=this;
        var status=this.data.status;

        that.setData({
            list: []
        });

        api.get({
            url: 'wxapp/game/getmytackbytype1',
            data: {status:status},
            success: data => {
                console.log(data.data);

                that.setData({
                    list: data.data
                });
            }
        });


    },


    getmytackbytype(){

        var that=this;
        var status=this.data.status;

        that.setData({
            list: []
        });

        api.get({
            url: 'wxapp/game/getmytackbytype',
            data: {status:status},
            success: data => {
                console.log(data.data);

                that.setData({
                    list: data.data
                });
            }
        });


    },

    exchangeq(ret){
        console.log(ret);
        var id=ret.currentTarget.dataset.id;

        wx.navigateTo({
          url: '/pages/user/gameszs?id='+id
        })
    }



})