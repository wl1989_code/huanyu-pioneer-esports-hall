var api = require('../../utils/api.js')
var app = getApp()



Page({

    /**
     * 页面的初始数据
     */
    data: {
        status:0,
        url:api.CURL,
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        this.getmymeallog();

    },

    getstatus(ret){
        console.log(ret);
        var i=ret.currentTarget.dataset.i;

        this.setData({
            status:i
        })

        this.getmymeallog();
    },


    getmymeallog(){
        var that=this;

        var status=this.data.status;

        that.setData({
            list: []
        });

        api.get({
            url: 'wxapp/game/getmymeallog',
            data: {status:status},
            success: data => {
                console.log(data.data);

                that.setData({
                    list: data.data
                });
            }
        });

    },
    exchangeq(){
        wx.showToast({
          title: '等待审核',
          icon:'none'
        })
    }



})