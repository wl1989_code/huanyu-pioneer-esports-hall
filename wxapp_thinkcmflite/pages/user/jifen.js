
var api = require('../../utils/api.js')
var app = getApp()


Page({

    /**
     * 页面的初始数据
     */
    data: {
        url:api.CURL,
        type:1
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        let jn = wx.getMenuButtonBoundingClientRect();

        console.log(jn);
        var tophe=jn.top;
   
        this.setData({
            tophe:tophe
        })

        this.getscoreinc();
    },

    seein(){

        this.setData({
            type:1
        })
        this.getscoreinc();
    },

    seeout(){

        this.setData({
            type:2
        })
        this.getscoredec();
    },

    //积分出
    getscoredec(){
        var that=this;

        that.setData({
            lista: []
        });

        api.get({
            url: 'wxapp/user/getscoredec',
            data: {},
            success: data => {
                console.log(data.data);

                that.setData({
                    lista: data.data
                });
            }
        });

    },

      //积分入
      getscoreinc(){
        var that=this;

        that.setData({
            lista: []
        });

        api.get({
            url: 'wxapp/user/getscoreinc',
            data: {},
            success: data => {
                console.log(data.data);

                that.setData({
                    lista: data.data
                });
            }
        });

    },

    onShow(){
        var that=this;

        api.get({
            url: 'wxapp/user/getUserInfo',
            data: {},
            success: data => {
                console.log(data.data);

                that.setData({user: data.data});
            }
        });

    },

    goback(){
        wx.navigateBack();
    }
})