
var api = require('../../utils/api.js')
var app = getApp()



Page({

    /**
     * 页面的初始数据
     */
    data: {
        status:0,
        url:api.CURL,
        info:'',
        scount:1,
        allcount:1,
        userid:''
    },

  

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        console.log(options.id)
        var that=this;
        api.post({
            url: 'wxapp/game/getinfobyid',
            data: {id:options.id},
            success: data => {
                console.log(data.data);

                if(data.code==0){

                    wx.showToast({
                      title: data.msg,
                      icon:'none'
                    })
                    wx.navigateBack();

                    return false;
                }
                

                that.setData({
                    info: data.data,
                    allcount:data.data.count,
                    id:data.data.id
                });
            }
        });
        
    },

    countdec(){
        var scount=this.data.scount;

        if(scount==1){

            scount=1;
        }else{
            scount=scount-1;

        }

        this.setData({
            scount:scount
        })
    },
    countadd(){
        var scount=this.data.scount;
        var allcount=this.data.allcount;
 
            scount=scount+1;

  

        this.setData({
            scount:scount
        })
    },
    getuserid(ret){
        this.setData({
            userid:ret.detail.value
        })
    },
    submits(){
        var id=this.data.id;
        var scount=this.data.scount;
        var uid=this.data.userid;
        api.post({
            url: 'wxapp/game/givetick',
            data: {id:id,scount:scount,uid:uid},
            success: data => {

                console.log(data);

                wx.showToast({
                  title: data.msg,
                  icon:'none'
                })

                if(data.code==1){
                    setTimeout(function(){
                        wx.navigateBack();
                    },1200)
                }
              
            }
        })

    }
    
})