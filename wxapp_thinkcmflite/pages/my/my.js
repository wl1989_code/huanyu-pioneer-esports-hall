var api = require('../../utils/api.js')

Page({
    data: {
        list: [],
        nickname:''
    },
    onLoad() {
        let jn = wx.getMenuButtonBoundingClientRect();

        console.log(jn);
        var tophe=jn.top;
        this.setData({
            tophe:tophe
        })
    },
    onShow() {
        var that=this;

        let user = wx.getStorageSync('user');
        if (!user) {
            wx.navigateTo({
                url: '/pages/login/login'
            });
        }

        api.get({
            url: 'wxapp/user/getUserInfo',
            data: {},
            success: data => {
                console.log(data.data);

                that.setData({user: data.data});
            }
        });

    

    },

    getqrcode(){
        // api.get({
        //     url: 'wxapp/getqrcode',
        //     data: {},
        //     success: data => {
        //        var img=data.data;

          
        //             wx.previewImage({
        //                 current: img, // 当前显示图片的http链接
        //                 urls: [img] // 需要预览的图片http链接列表
        //             });
            
                

        //     }
        // });

    },
    nexturl(ret){
        console.log(ret);
        var url=ret.currentTarget.dataset.url;

        wx.navigateTo({
          url: url
        })

    },

    outlogin(){

        wx.setStorageSync('token', '')
        wx.setStorageSync('user', '')
        wx.navigateTo({
          url: '/pages/login/login',
        })

    }
});