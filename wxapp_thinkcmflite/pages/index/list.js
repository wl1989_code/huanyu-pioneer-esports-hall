var api = require('../../utils/api.js');
var app = getApp();


Page({

    /**
     * 页面的初始数据
     */
    data: {
        list:[]
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onShow() {

        var that=this;
        api.post({
            url: 'wxapp/index/getallgame',
            data: {},
            success: data => {

                that.setData({
                    list:data.data
                })
            }
        })


    },

    seeinfo(ret){

        console.log(ret);

        var id=ret.currentTarget.dataset.id;

        wx.navigateTo({
          url: '/pages/game/index?id='+id,
        })
    }

})