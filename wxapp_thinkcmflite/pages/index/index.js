var api = require('../../utils/api.js');
var app = getApp();
Page({
    data: {
        url:api.CURL,
        tophe:0,
        list:[],
        game:[],
        show:0,
        number:1,
        allscore:0
    },
    currentPageNumber: 1,
    onLoad() {
     
    },


     /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh() {

        var that=this;

        setTimeout(function(){

            that.getindexgame();
            that.getindexticket();
        },500)
       
    },



    onShow() {
        let jn = wx.getMenuButtonBoundingClientRect();

        console.log(jn);
        var tophe=jn.top+jn.height;
        var over =tophe+230 ;
        this.setData({
            tophe:tophe,
            over:over+ 'px'
        })

        this.getbanner();
        this.getindexgame();
        this.getindexticket();
     
    },

    /**
     * 幻灯哦
     */
    getbanner(){
        var that=this;
        api.post({
            url: 'wxapp/index/getbanner',
            data: {},
            success: data => {

                that.setData({
                    banner:data.data
                })
            }
        })

    },
    
    seeall(){
        wx.switchTab({
          url: '/pages/all/index',
        })
    },
    seelist(){
        wx.navigateTo({
    
            url: '/pages/index/list',
          })
    },
    // 相关比赛
    getindexgame(){
        var that=this;
        api.post({
            url: 'wxapp/index/getindexgame',
            data: {},
            success: data => {

                that.setData({
                    game:data.data
                })
            }
        })

    },

        // 比赛券
        getindexticket(){
            var that=this;
            api.post({
                url: 'wxapp/index/getindexticket',
                data: {},
                success: data => {
    
                    that.setData({
                        list:data.data
                    })
                }
            })
    
        },

    seegameinfo(ret){
        console.log(ret);

        var id=ret.currentTarget.dataset.id;

        wx.navigateTo({
          url: '/pages/game/index?id='+id
        })


    },

    //兑换券

    exchangeq(ret){

        var id=ret.currentTarget.dataset.id;
        var score=ret.currentTarget.dataset.score;

        this.setData({

            allscore:score,
            selectid:id,
            score:score,
            show:1

        })

    },

    setshow0(){
        this.setData({
            show:0,
            number:1
        })
    },
    numberinc(){
        var number=this.data.number;
        var score=this.data.score;

        if(number==1){
            number=1;
        }else{
            number=number-1;

        }

        var allscore=number*score;


        this.setData({
            number:number,
            allscore:allscore
        })



    },
    numberadd(){
        var number=this.data.number;
        var score=this.data.score;
        number=number+1;
        var allscore=number*score;
        this.setData({
            number:number,
            allscore:allscore
        })

    },

    setshow1(){

        var that=this;


        that.setData({
            show:0
        })

        var id=this.data.selectid;
        var number=this.data.number;
        var allscore=this.data.allscore;
        api.post({
            url: 'wxapp/game/exchangegame',
            data: {id:id,number:number,allscore:allscore},
            success: data => {


                wx.showToast({
                  title: data.msg,
                  icon:'none'
                })
              console.log(data);

            }
        })




    },


});
