

var api = require('../../utils/api.js');
var app = getApp();



Page({

    /**
     * 页面的初始数据
     */
    data: {
        info:[],
        game:[]
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {


        var that=this;
        api.post({
            url: 'wxapp/index/getgamebyid',
            data: {id:options.id},
            success: data => {

                that.setData({
                    info:data.data
                })
            }
        })



        this.getindexgame();
    },

    getindexgame(){
        var that=this;
        api.post({
            url: 'wxapp/index/getindexgame',
            data: {},
            success: data => {

                that.setData({
                    game:data.data
                })
            }
        })

    },

    seegameinfo(ret){
        console.log(ret);

        var id=ret.currentTarget.dataset.id;

        wx.navigateTo({
          url: '/pages/game/index?id='+id
        })


    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady() {

    },

    baoming(){
        wx.showToast({
          title: '请到线下门店参与报名!',
          icon:'none'
        })
    }

})