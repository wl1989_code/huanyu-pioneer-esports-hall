var api = require('../../utils/api.js');
var app = getApp();
Page({
    data: {
      imgs:'',
      txt:'',
      id:''
    },
    currentPageNumber: 1,
    onLoad() {
      var usrl=api.HOST;
       console.log(usrl);
    },
    onShow() {
        
    },



    getimgs(){
        let that = this;
        var usrl=api.HOST;

        wx.chooseImage({
          count: 1,
          sizeType: ['compressed'], // 可以指定是原图还是压缩图，默认二者都有
          sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
          success: function (res) {
            console.log(res)
      
            // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
            let tempFilePaths = res.tempFilePaths;
            var token=wx.getStorageSync('token')
         
            wx.uploadFile({
              filePath: tempFilePaths[0],
              name: 'file',
              url: usrl+'common/upload',
              formData:{token:token},
              success:function(res){
                var data= JSON.parse(res.data);
                that.setData({
                  imgs:data.data.fullurl
                });
        
              },
              fail: function (res) {
                console.log('上传失败');
                }
            })
          }
        })
    },

});
