
var api = require('../../utils/api.js')
var app = getApp()

Page({

    /**
     * 页面的初始数据
     */
    data: {
        url:api.CURL,
        list:[],
        game:[],
        show:0,
        number:1,
        allscore:0
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad(options) {
        let jn = wx.getMenuButtonBoundingClientRect();

        console.log(jn);
        var tophe=jn.top;
   
        this.setData({
            tophe:tophe,
    
        })

        this.getallticket();
    },

        // 比赛券
        getallticket(){
        var that=this;
        api.post({
            url: 'wxapp/index/getallticket',
            data: {},
            success: data => {

                that.setData({
                    list:data.data
                })
            }
        })

    },



    //兑换券

    exchangeq(ret){

        var id=ret.currentTarget.dataset.id;
        var score=ret.currentTarget.dataset.score;

        this.setData({

            allscore:score,
            selectid:id,
            score:score,
            show:1

        })

    },

    setshow0(){
        this.setData({
            show:0,
            number:1
        })
    },
    numberinc(){
        var number=this.data.number;
        var score=this.data.score;

        if(number==1){
            number=1;
        }else{
            number=number-1;

        }

        var allscore=number*score;


        this.setData({
            number:number,
            allscore:allscore
        })



    },
    numberadd(){
        var number=this.data.number;
        var score=this.data.score;
        number=number+1;
        var allscore=number*score;
        this.setData({
            number:number,
            allscore:allscore
        })

    },

    setshow1(){

        var that=this;


        that.setData({
            show:0
        })

        var id=this.data.selectid;
        var number=this.data.number;
        var allscore=this.data.allscore;
        api.post({
            url: 'wxapp/game/exchangegame',
            data: {id:id,number:number,allscore:allscore},
            success: data => {


                wx.showToast({
                  title: data.msg,
                  icon:'none'
                })
              console.log(data);

            }
        })




    },



})