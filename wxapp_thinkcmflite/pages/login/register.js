var api = require('../../utils/api.js')
var app = getApp()
Page({
    data: {
        code: '',
        openid: '',
        nickname: '',
        url: api.CURL,
        'yzmcode': '获取验证码'

    },

    onLoad() {

        let jn = wx.getMenuButtonBoundingClientRect();

        console.log(jn);
        var tophe = jn.top;

        this.setData({
            tophe: tophe
        })


    },
    onShow() {
        //调用登录接口
        var that = this;




    },

    getmobile(ret) {
        this.setData({
            mobile: ret.detail.value
        })
    },
    getcode(ret) {
        this.setData({
            code: ret.detail.value
        })
    },

    getsms() {

        var that = this;
        var mobile = this.data.mobile;

        var yzmcode = this.data.yzmcode;

        if (yzmcode != '获取验证码') {
            return false;
        }

        api.post({
            url: 'user/public/send',
            data: {
                'username': mobile
            },
            success: data => {
                console.log(data);

                wx.showToast({
                    title: data.msg,
                    icon: 'none'
                })

                if (data.code == 1) {

                    that.startdjs();
                }


            }
        })



    },

    startdjs() {

        var s = 60;
        var that = this;

        var index = setInterval(function () {

            s = s - 1;

            if (s < 1) {
                that.setData({
                    yzmcode: '获取验证码'
                })
                clearInterval(index);
            } else {
                that.setData({
                    yzmcode: s + 's后获取'
                })
            }


        }, 1000)

    },

    gologin() {
        wx.reLaunch({
            url: '/pages/login/login'
        })
    },


    //注册

    register() {

        var mobile = this.data.mobile;
        var code = this.data.code;
        var that = this;

        api.post({
            url: 'user/public/register',
            data: {
                'username': mobile,
                'verification_code': code
            },
            success: data => {
                console.log(data);

                wx.showToast({
                    title: data.msg,
                    icon: 'none'
                })

                if (data.code == 1) {

                    wx.reLaunch({
                        url: '/pages/login/login',
                    })
                }

            }
        })


    }


})