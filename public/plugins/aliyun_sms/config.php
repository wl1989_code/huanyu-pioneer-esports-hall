<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2017 http://xiaoji.name All rights reserved.
// +----------------------------------------------------------------------
// | Author: jackie <297129853@qq.com>
// +----------------------------------------------------------------------
return array(
    'accessKeyId' => array(
        'title' => 'accessKeyId',
        'type' => 'text',
        'value' => ''
    ),
    'accessKeySecret' => array(
        'title' => 'accessKeySecret',
        'type' => 'text',
        'value' => ''
    ),
    'signName' => array(
        'title' => '短信签名',
        'type' => 'text',
        'value' => ''
    ),
    'templateId' => array(
        'title' => '短信模板ID',
        'type' => 'text',
        'value' => ''
    ),
    'expire_minute' => array(// 在后台插件配置表单中的键名 ,会是config[text]
        'title' => '有效期', // 表单的label标题
        'type' => 'text',// 表单的类型：text,password,textarea,checkbox,radio,select等
        'value' => '30',// 表单的默认值
        'tip' => '短信验证码过期时间，单位分钟' //表单的帮助提示
    ),
);
