<?php
// +----------------------------------------------------------------------
// | Copyright (c) 2017 http://xiaoji.name All rights reserved.
// +----------------------------------------------------------------------
// | Author: jackie <297129853@qq.com>
// +----------------------------------------------------------------------
namespace plugins\aliyun_sms;

use cmf\lib\Plugin;

class AliyunSmsPlugin extends Plugin
{
    public $info = array(
        'name'=>'AliyunSms',
        'title'=>'阿里云手机验证码',
        'description'=>'阿里云手机验证码',
        'status'=>1,
        'author'=>'jackie',
        'version'=>'1.0'
    );
    
    public $has_admin=0;//插件是否有后台管理界面
    
    public function install()
    {//安装方法必须实现
        return true;//安装成功返回true，失败false
    }
    
    public function uninstall()
    {//卸载方法必须实现
        return true;//卸载成功返回true，失败false
    }
    
    //实现的send_mobile_verification_code钩子方法
    public function sendMobileVerificationCode($param)
    {
        $mobile        = $param['mobile'];//手机号
        $code          = $param['code'];//验证码
        $config        = $this->getConfig();
        $expire_minute = intval($config['expire_minute']);
        $expire_minute = empty($expire_minute) ? 30 : $expire_minute;
        $expire_time   = time() + $expire_minute * 60;
        $result        = false;
		//dump($config['accessKeyId']);exit;
        //send message
        if ($code!==false) {
           // $params = array(
            //    "$code",
			//	"$expire_minute"
           // );
             
			// *** 需用户填写部分 ***
			// fixme 必填：是否启用https
			$security = false;
			$accessKeyId = $config['accessKeyId'];
			$accessKeySecret = $config['accessKeySecret'];
			$params["PhoneNumbers"] = $mobile;//用户手机号码
			$params["SignName"] = $config['signName'];
			$params["TemplateCode"] = $config['templateId'];
			// fixme 可选: 设置模板参数, 假如模板中存在变量需要替换则为必填项
			$params['TemplateParam'] = Array (
				"code" => $code,
			);
			// *** 需用户填写部分结束, 以下代码若无必要无需更改 ***
			if(!empty($params["TemplateParam"]) && is_array($params["TemplateParam"])) {
				$params["TemplateParam"] = json_encode($params["TemplateParam"], JSON_UNESCAPED_UNICODE);
			}
			
			 
			// 初始化SignatureHelper实例用于设置参数，签名以及发送请求
			$helper = new \plugins\aliyun_sms\lib\SignatureHelper();
			 
			// 此处可能会抛出异常，注意catch
			$content = $helper->request(
				$accessKeyId,
				$accessKeySecret,
				"dysmsapi.aliyuncs.com",
				array_merge($params, array(
					"RegionId" => "cn-hangzhou",
					"Action" => "SendSms",
					"Version" => "2017-05-25",
				)),
				$security
			);
			//dump($content);exit;
			// $res = json_decode($content);
				if($content->Code=='OK' && $content->Message=='OK'){
					 $result = array(
						'error'=>0,
						'message'=>'发送成功！',
						'data'=>array($content->Code,$content->Message)
					);
					cmf_verification_code_log($mobile, $code, $expire_time);
				}else{
					 $result = array(
						'error'=>1,
						'message'=> '发送失败！',
						'data'=>array($content->Code,$content->Message)
					 );
				}
			//dump($content) ;
			
			
        } else {
            $result = array(
                'error'=>1,
                'message'=>'发送次数过多，不能再发送'
            );
        }
        return $result;
    }
}
