<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Powerless < wzxaini9@gmail.com>
// +----------------------------------------------------------------------

namespace app\user\controller;

use cmf\controller\AdminBaseController;
use think\Db;
use think\db\Query;

/**
 * Class AdminIndexController
 * @package app\user\controller
 *
 * @adminMenuRoot(
 *     'name'   =>'用户管理',
 *     'action' =>'default',
 *     'parent' =>'',
 *     'display'=> true,
 *     'order'  => 10,
 *     'icon'   =>'group',
 *     'remark' =>'用户管理'
 * )
 *
 * @adminMenuRoot(
 *     'name'   =>'用户组',
 *     'action' =>'default1',
 *     'parent' =>'user/AdminIndex/default',
 *     'display'=> true,
 *     'order'  => 10000,
 *     'icon'   =>'',
 *     'remark' =>'用户组'
 * )
 */
class AdminIndexController extends AdminBaseController
{

    /**
     * 后台本站用户列表
     */
    public function index()
    {
        if ($this->request->isPost()){

            $limit=$this->request->param('limit');
            $page=$this->request->param('page');
            $limits=($page-1)*$limit .  ','. $limit;

            $map[]=['user_type','eq',2];

            $name=$this->request->param('name');
            if ($name){
                $map[]=['id','like','%'.$name.'%'];
            }

            $mobile=$this->request->param('mobile');
            if ($mobile){
                $map[]=['mobile','like','%'.$mobile.'%'];
            }


            $scorea=$this->request->param('scorea');
            if ($scorea){
                $map[]=['score','gt',$scorea];
            }

            $scoreb=$this->request->param('scoreb');
            if ($scoreb){
                $map[]=['score','lt',$scoreb];
            }

            $list=Db::name('user')
                ->where($map)
                ->limit($limits)
                ->order('id desc')
                ->select()->toArray();

//            $ret=Db::getLastSql();
//            echo $ret;

            $count=Db::name('user')
                ->where($map)
                ->count();

            foreach ($list as &$val){

                $val['shiming']=$val['car_name']?'实名':'未实名';
                $val['create_time']=date('Y-m-d H:i',$val['create_time']);


            }


            $counts=Db::name('user')
                ->where($map)
                ->sum('score');

            echo json_encode(['code'=>0,'msg'=>$counts,'count'=>$count,'data'=>$list]);
            die;

        }

        return $this->fetch();
    }

    /**
     * 本站用户拉黑

     */
    public function delete()
    {
        $id = $this->request->param('id');

            $result = Db::name("user")->where(["id" => $id, "user_type" => 2])->delete();

            $this->success('操作成功！');

    }
    /*  实名 */
    public function shiming(){

        if ($this->request->isPost()){
            $arrData  = $this->request->post('post');

                $id=$arrData['id'];
                unset($arrData['id']);


            $arrData['user_nickname']=$arrData['car_name'];

            Db::name('user')->where(['id'=>$id])->update($arrData);

            $this->success("操作成功");
        }

        $id = $this->request->param('id');
        $result = Db::name("user")->where(["id" => $id, "user_type" => 2])->find();

        $result['car_images']=cmf_get_image_url($result['car_img']);

        $this->assign('user',$result);
        return $this->fetch();

    }

    //用户积分

    public function scorelog(){


        if ($this->request->isPost()){

            $id=$this->request->param('uid');


            $list=Db::name('user_score_log')->where(['user_id'=>$id])->order('id desc')->select()->toArray();

            foreach ($list as &$val){

                $val['create_time']=date('Y-m-d H:i',$val['create_time']);
            }

            echo json_encode(['code'=>0,'msg'=>'','count'=>count($list),'data'=>$list]);
            die;


        }

        $id = $this->request->param('id');
        $this->assign('id',$id);
        return $this->fetch();




    }

    //兑换券

    public function ticket(){


        if ($this->request->isPost()){

            $id=$this->request->param('uid');
            $map[] = ['a.user_id', 'eq', $id];

            $list=Db::name('ticket_log')
                ->alias('a')

                ->join('cmf_ticket c','c.id=a.ticket_id')
                ->where($map)
                ->field(['a.*','c.name'])
                ->order('a.id desc')

                ->select()->toArray();

            echo json_encode(['code'=>0,'msg'=>'','count'=>count($list),'data'=>$list]);
            die;

        }

        $id = $this->request->param('id');
        $this->assign('id',$id);
        return $this->fetch();



    }
    //充值

    public function congzi(){

        $id=$this->request->param('id');
        $value=$this->request->param('value');

        if ($value<1){

            $this->error('金额错误');
        }

       $logid=scoresetinc($id,$value,'后台充值');

        $this->success('操作成功','',['logid'=>$logid]);
    }

    public function kouchu(){

        $id=$this->request->param('id');
        $number=$value=$this->request->param('value');

        if ($value<1){

            $this->error('金额错误');
        }

        $user=Db::name('user')->where(['id'=>$id])->find();

        if ($user['score']<$number){

            $this->error('金额错误');
        }




        $logid=scoresetdec($id,$value,'后台扣除');

        $this->success('操作成功','',['logid'=>$logid]);
    }


    //打印信息

    public function print(){

        $id=$this->request->param('id');
        $info=Db::name('user_score_log')->where(['id'=>$id])->find();

        if (!$info){

            $info=Db::name('user_score_log')->order('id desc')->find();
        }


        $user=Db::name('user')->where(['id'=>$info['user_id']])->find();
        $info['uname']=$user['user_nickname'];


        $this->assign('info',$info);
        return $this->fetch();

    }

    public function printb(){

        $id=$this->request->param('id');

        $info=Db::name('game_log')->where(['id'=>$id])->find();

        $user=Db::name('user')->where(['id'=>$info['user_id']])->find();
        $info['uname']=$user['user_nickname'];

        $info['times']=$info['times']?$info['times']:time();
        $this->assign('info',$info);
        return $this->fetch();

    }

    public function printd(){

        $id=$this->request->param('id');

        $infos=Db::name('game_log')->where('id','in',$id)->select()->toArray();
        $info=$infos[0];

        $user=Db::name('user')->where(['id'=>$info['user_id']])->find();
        $info['uname']=$user['user_nickname'];

        $info['times']=$info['times']?$info['times']:time();

        $info['count']=count($infos);
        $this->assign('info',$info);
        return $this->fetch();

    }
        //赠送券
    public function printc(){


        $id=$this->request->param('id');
        $info=Db::name('ticket_log_l')->where(['id'=>$id])->find();

        $user=Db::name('user')->where(['id'=>$info['user_id']])->find();
        $info['uname']=$user['user_nickname'];

        $ticket=Db::name('ticket')->where(['id'=>$info['ticket_id']])->find();

        $info['ticket_name']=$ticket['name'];

        $this->assign('info',$info);
        return $this->fetch();

    }

    //zengsong
    public function zengsong(){

        $id=$this->request->param('id');
        $value=$this->request->param('value');

        if ($value<1){

            $this->error('金额错误');
        }

        $logid=scoresetinc($id,$value,'后台赠送');

        $this->success('操作成功','',['logid'=>$logid]);
    }

    /**
     * 报名页
     */
    public function baoming(){

        $id=$this->request->param('id');

        $game=Db::name('game')->where(['status'=>1])->select()->toArray();

        $this->assign('id',$id);
        $this->assign('games',$game);
        
        return $this->fetch();

    }


    //获取用户的券

    public function getlist(){

        $uid=$this->request->param('uid');
        $id=$this->request->param('id');


        $game=Db::name('game')->where('id','eq',$id)->find();

        $logs=Db::name('ticket_log')
            ->where(['ticket_id'=>$game['ticket_id'],'user_id'=>$uid,'status'=>0])
            ->where('count','gt',0)
            ->select()->toArray();



        $tik=Db::name('ticket')->where(['id'=>$game['ticket_id']])->find();

        foreach ($logs as  &$key){
            $key['name']=$tik['name'];
        }



        echo json_encode($logs);
        die;


    }



    /**
     * 参加比赛
        uid: 2  用户id
        stype: 2  1=积分  2 =兑换
        mid: 1  gameiud
        tid: 4  券 id
     * */
    public function joingame(){

        $uid=$this->request->param('uid');
        $stype=$this->request->param('stype');
        $mid=$this->request->param('mid');
        $tid=$this->request->param('tid');
        $numner=$this->request->param('numner');


        $user=Db::name('user')->where(['id'=>$uid])->find();
        if (!$user['car_name']){
            $this->error('用户未实名!');

        }

        $game=Db::name('game')->where('id','eq',$mid)->find();


        if (!$game){
            $this->error('无信息');
        }

        //参数券兑换
        if ($stype==2){

            $ticketlog=Db::name('ticket_log')
                ->where(['id'=>$tid])
                ->where(['status'=>0])
                ->where(['user_id'=>$uid])
                ->find();

            if (!$ticketlog){
                $this->error('没有券');
            }


            if($ticketlog['count']<$numner){
                $this->error('可用券不足');
            }

            $ticket=Db::name('ticket')->where(['id'=>$ticketlog['ticket_id']])->find();


            $send=$ticketlog['count']-$numner;


            //数量减少
            Db::name('ticket_log')->where(['id'=>$ticketlog['id']])->update(['count'=>$send]);


            $logb['user_id']=$uid;
            $logb['game_name']=$game['name'];
            $logb['game_id']=$game['id'];
            $loga['scorec']=$game['scorec'];
            $logb['stype']=2;
            $logb['ticket_log_id']=$tid;
            $logb['ticket_log_name']=$ticket['name'];
            $logb['times']=time();
            $logb['number']=$numner;


            $logid= Db::name('game_log')->insertGetId($logb); //参赛记录



            $logc['user_id']=$ticketlog['user_id'];
            $logc['ticket_id']=$ticketlog['ticket_id'];
            $logc['f_user']=1;
            $logc['count']=$numner;
            $logc['score']=0;
            $logc['get']=3;
            $logc['status']=2;
            $logc['desc']='后台参赛';
            $logc['gettime']=time();

            Db::name('ticket_log_l')->data($logc)->insert();


            $this->success('操作成功!','',['logid'=>trim($logid,',')]);

        }


        //积分兑换
        if ($stype==1){

            if ($numner<1){
                $this->error('请选择数量');
            }

            $scores=$game['score']*$numner;

            if ($user['score']<$scores){
                $this->error('积分不足');
            }


            $loga['user_id']=$uid;
            $loga['game_name']=$game['name'];
            $loga['game_id']=$game['id'];
            $loga['stype']=$stype;
            $loga['score']=$scores;
            $loga['number']=$numner;
            $loga['scorec']=$game['scorec']*$numner;
            $loga['times']=time();

            Db::name('game_log')->data($loga)->insert();
            $logid=scoresetdec($uid,$scores,'参加比赛');

            $this->success('操作成功','',['logid'=>$logid]);

        }




    }

    //参赛记录

    public function game(){
        if ($this->request->isPost()){

            $id=$this->request->param('uid');
            $map[] = ['a.user_id', 'eq', $id];

            $list=Db::name('game_log')
                ->alias('a')
                ->join('cmf_game c','c.id=a.game_id')
                ->where($map)
                ->field(['a.*','c.name'])
                ->order('a.id desc')

                ->select()->toArray();

            foreach ($list as &$key){

                $key['times']=date('Y-m-d H:i',$key['times']);
            }

            echo json_encode(['code'=>0,'msg'=>'','count'=>count($list),'data'=>$list]);
            die;

        }

        $id = $this->request->param('id');
        $this->assign('id',$id);
        return $this->fetch();

    }
}
