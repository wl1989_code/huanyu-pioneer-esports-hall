<?php
/**
 *Title:
 *<br/>By: 王龙 2022/8/29 16:06
 */


namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;
use app\admin\model\AdminMenuModel;

class TestController extends AdminBaseController{

    public function index() {

        return $this->fetch();


    }

    /**
     * 表单
     */

    public function add(){

        return $this->fetch();

    }

    /**
     * 获取数据
     */

    public function getlist(){

        $limit=$this->request->param('limit');
        $page=$this->request->param('page');
        $limits=($page-1)*$limit .  ','. $limit;


        $key=$this->request->param('key');

        $map[]=['id','gt','0'];
        $map[]=['user_type','eq','1'];

        $list=Db::name('user')
            ->where($map)
            ->limit($limits)
            ->select()->toArray();

        $ret=Db::getLastSql();



        echo json_encode(['code'=>0,'msg'=>'','count'=>'10','data'=>$list]);
        die;


    }


}