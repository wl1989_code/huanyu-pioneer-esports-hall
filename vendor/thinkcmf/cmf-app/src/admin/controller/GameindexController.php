<?php
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;
use app\admin\model\AdminMenuModel;

class GameindexController extends AdminBaseController
{

    public function index()
    {
        if ($this->request->isPost()){

            $limit=$this->request->param('limit');
            $page=$this->request->param('page');
            $limits=($page-1)*$limit .  ','. $limit;

            $map[]=['id','gt','0'];



            $status=$this->request->param('status');
            $speak=$this->request->param('speak');
            $name=$this->request->param('name');

            if ($status>0){
                $map[]=['status','eq',$status-1];
            }
            if ($speak){
                $map[]=['speak','like','%'.$speak.'%'];
            }

            if ($status){
                $map[]=['name','like','%'.$name.'%'];
            }



            $list=Db::name('game')
                ->where($map)
                ->limit($limits)
                ->order('id desc')
                ->select()->toArray();



            foreach ($list as &$val){

                $count=Db::name('game_log')->where(['game_id'=>$val['id']])->count();
                $val['inuser']=$count;

            }

            $counts=Db::name('game')
                ->where($map)->count();
            echo json_encode(['code'=>0,'msg'=>'','count'=>$counts,'data'=>$list]);
            die;

        }


        return $this->fetch();

    }


    //删除

    public  function delete(){

        $id  = $this->request->post('id');


        Db::name('game')->where(['id'=>$id])->delete();

        $this->success("操作成功！");
    }

    //添加信息
    public function add(){

        $ticket=Db::name('ticket')->where(['status'=>1])->select()->toArray();

        $this->assign("ticket", $ticket);
        return $this->fetch();
    }

    public function edit(){

        $id  = $this->request->param('id');
        $info=Db::name('game')->where(['id'=>$id])->find();
        $info['images']=cmf_get_image_url($info['image']);
        $info['starttime']=date('Y-m-d H:i:s',$info['starttime']);
        $info['endtime']=date('Y-m-d H:i:s',$info['endtime']);


        $this->assign("info", $info);

        return $this->fetch();
    }

    //添加修改数据

    public function addPost(){


        $arrData  = $this->request->post('post');

        $arrData['starttime']=strtotime($arrData['starttime']);
        $arrData['endtime']=strtotime($arrData['endtime']);

        if (isset($arrData['id'])&&$arrData['id']>0){

            $id=$arrData['id'];
            unset($arrData['id']);
            Db::name('game')->where(['id'=>$id])->update($arrData);

            $this->success("操作成功！",'gameindex/index');
        }else{
            Db::name('game')->data($arrData)->insert();

            $this->success("操作成功！",'gameindex/index');

        }

    }

    // 列表

    public function inuser(){

        if ($this->request->isPost()){

            $id=$this->request->param('id');
            $count=Db::name('game')->where(['id'=>$id])->find();

            $list=Db::name('game_log')
                ->alias('a')
                ->join('cmf_user b','a.user_id=b.id')
                ->where(['a.game_id'=>$id])
//                ->where('a.status','neq',2)
                ->field(['a.*','b.user_nickname'])
                ->order('a.id desc')
                ->select()->toArray();

            foreach ($list as &$val){
                $val['game_name']=$count['name'];
            }


            echo json_encode(['code'=>0,'msg'=>'32132','count'=>'100','data'=>$list]);
            die;
        }




        $id=$this->request->param('id');

        $this->assign("id", $id);

        return $this->fetch();

    }

}


