<?php

namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;
use app\admin\model\AdminMenuModel;
use wxapp\aes\ErrorCode;

class MeallogController extends AdminBaseController
{

    public function index()
    {
        if ($this->request->isPost()) {

            $limit = $this->request->param('limit');
            $page = $this->request->param('page');
            $limits = ($page - 1) * $limit . ',' . $limit;

            $map[] = ['a.id', 'gt', 0];

            $status = $this->request->param('status','-1');
            $name = $this->request->param('name');
            $user = $this->request->param('user');
            $get= $this->request->param('gets','-1');

            if ($get == 0) {
                $map[] = ['a.get', 'eq', 0];
            }else if ($get == 1) {
                $map[] = ['a.get', 'eq', 1];
            }else if ($get == 2) {
                $map[] = ['a.get', 'eq', 2];
            }

            if ($status == 0) {
                $map[] = ['a.status', 'eq', 0];
            }else if ($status == 1) {
                $map[] = ['a.status', 'eq', 1];
            }else if ($status == 3) {
                $map[] = ['a.status', 'eq', 3];
            }else if ($status == 2) {
                $map[] = ['a.status', 'eq', 2];
            }

            if ($name) {
                $map[] = ['c.name', 'like', '%' . $name . '%'];
            }

            if ($user) {
                $map[] = ['c.user_nickname', 'like', '%' . $user . '%'];
            }

            $id=$this->request->param('id');


            $list=Db::name('meal_log')
                ->alias('a')
                ->join('cmf_user b','a.user_id=b.id')
                ->join('cmf_meal c','c.id=a.meal_id')
                ->where($map)
                ->field(['a.*','b.user_nickname','c.name'])
                ->order('a.id desc')
                ->limit($limits)
                ->select()->toArray();

            foreach ($list as &$val){
                $val['endtime']=date('y-m-d',$val['endtime']);
            }


            $count=   Db::name('meal_log')
                ->alias('a')
                ->join('cmf_user b','a.user_id=b.id')
                ->join('cmf_meal c','c.id=a.meal_id')
                ->where($map)
                ->sum('a.score');

            $counts=   Db::name('meal_log')
                ->alias('a')
                ->join('cmf_user b','a.user_id=b.id')
                ->join('cmf_meal c','c.id=a.meal_id')
                ->where($map)
                ->count();


            echo json_encode(['code' => 0, 'msg' =>$count, 'count' => $counts, 'data' => $list]);
            die;

        }


        return $this->fetch();

    }

}