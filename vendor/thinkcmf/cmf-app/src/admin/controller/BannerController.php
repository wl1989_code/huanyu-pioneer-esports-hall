<?php
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;
use app\admin\model\AdminMenuModel;

class BannerController extends AdminBaseController
{

    public function index()
    {

        if ($this->request->isPost()){

            $limit=$this->request->param('limit');
            $page=$this->request->param('page');
            $limits=($page-1)*$limit .  ','. $limit;
            $key=$this->request->param('key');


            $map[]=['id','gt','0'];


            $list=Db::name('banner')
                ->where($map)
                ->limit($limits)
                ->select()->toArray();

            $count=Db::name('banner')
                ->where($map)->count();

//            $ret=Db::getLastSql();

            foreach ($list as &$val){

                $val['image']=cmf_get_image_url($val['image']);

            }

            echo json_encode(['code'=>0,'msg'=>'','count'=>$count,'data'=>$list]);
            die;

        }


        return $this->fetch();
    }





    public function add()
    {

        return $this->fetch();
    }

    public function edit(){


        $id  = $this->request->param('id');

        $info= Db::name('banner')->where(['id'=>$id])->find();



        $info['images']=cmf_get_image_url($info['image']);





        $this->assign("info", $info);

        return $this->fetch();

    }

    public function delete(){

        $id  = $this->request->post('id');

        Db::name('banner')->where(['id'=>$id])->delete();

        $this->success("操作成功！",'banner/index');


    }

    public function addPost(){



        $arrData  = $this->request->post('post');


        if (isset($arrData['id'])&&$arrData['id']>0){

            $id=$arrData['id'];
            unset($arrData['id']);
            Db::name('banner')->where(['id'=>$id])->update($arrData);

            $this->success("操作成功！",'banner/index');
        }

        Db::name('banner')->data($arrData)->insert();

        $this->success("操作成功！",'banner/index');


    }


}

