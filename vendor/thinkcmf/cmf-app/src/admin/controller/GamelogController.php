<?php
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;
use app\admin\model\AdminMenuModel;

class GamelogController extends AdminBaseController
{

    public function index()
    {
        if ($this->request->isPost()) {

            $limit = $this->request->param('limit');
            $page = $this->request->param('page');
            $limits = ($page - 1) * $limit . ',' . $limit;

            $map[] = ['a.id', 'gt', '0'];

            //username,gamename:gamename,start_time:start_time,end_time

            $username= $this->request->param('username');
            $game_name = $this->request->param('game_name');
            $start_time = $this->request->param('start_time');
            $end_time= $this->request->param('end_time');
            if ($username) {
                $map[] = ['b.mobile', 'like', '%'.$username.'%'];
            }

            if ($game_name) {
                $map[] = ['a.game_name', 'like', '%'.$game_name.'%'];
            }
            if ($start_time) {
                $map[] = ['a.times', 'gt', strtotime($start_time)];
            }
            if ($start_time) {
                $map[] = ['a.times', 'lt', strtotime($end_time)];
            }


            $list = Db::name('game_log')
                ->alias('a')
                ->join('cmf_user b','a.user_id=b.id')
                ->where($map)
                ->limit($limits)
                ->field(['a.*','b.user_nickname'])
                ->order('a.id desc')
                ->select()->toArray();


            foreach ($list as &$val) {

                $val['times'] = date('y-m-d H:i',$val['times']);

            }

            $counts = Db::name('game_log')
                ->alias('a')
                ->join('cmf_user b','a.user_id=b.id')
                ->where($map)->count();

            $score = Db::name('game_log')
                ->alias('a')
                ->join('cmf_user b','a.user_id=b.id')
                ->where($map)->sum('a.score');

            $scorec= Db::name('game_log')
                ->alias('a')
                ->join('cmf_user b','a.user_id=b.id')
                ->where($map)->sum('a.scorec');

            $str='总计使用 :'.$score.' 积分,成本积分 : '.$scorec;

            echo json_encode(['code' => 0, 'msg' => $str, 'count' => $counts, 'data' => $list]);
            die;

        }


        return $this->fetch();

    }
}