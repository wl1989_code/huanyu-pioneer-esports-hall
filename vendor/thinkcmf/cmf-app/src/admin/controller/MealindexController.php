<?php
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;
use app\admin\model\AdminMenuModel;

class MealindexController extends AdminBaseController
{

    public function index()
    {
        if ($this->request->isPost()) {

            $limit = $this->request->param('limit');
            $page = $this->request->param('page');
            $limits = ($page - 1) * $limit . ',' . $limit;

            $map[] = ['id', 'gt', '0'];

            //username,gamename:gamename,start_time:start_time,end_time

            $name= $this->request->param('name');
            if ($name) {
                $map[] = ['name', 'like', '%'.$name.'%'];
            }

            $status= $this->request->param('status','-1');

            if ($status==0) {
                $map[] = ['status', 'eq', 0];
            }else if ($status==1) {
                $map[] = ['status', 'eq', 1];
            }


            $list = Db::name('meal')
                ->where($map)
                ->limit($limits)
                ->order('id desc')
                ->select()->toArray();

            $ret=Db::getLastSql();

            foreach ($list as &$val) {

                $val['starttime'] = date('y-m-d H:i',$val['starttime']);
                $val['endtime'] = date('y-m-d H:i',$val['endtime']);
                $val['inuser'] =Db::name('meal_log')->where(['meal_id'=>$val['id']])->count();
            }

            $counts = Db::name('meal')
                ->where($map)->count();

            echo json_encode(['code' => 0, 'msg' => '', 'count' => $counts, 'data' => $list]);
            die;

        }


        return $this->fetch();

    }

    /*
     * 添加
     * */
    public function add(){
        return $this->fetch();
    }


    /*
     * 查看
     * */
    public function seelog(){

        if ($this->request->isPost()){

            $id=$this->request->param('id');
            $count=Db::name('ticket')->where(['id'=>$id])->find();

            $list=Db::name('meal_log')
                ->alias('a')
                ->join('cmf_user b','a.user_id=b.id')
                ->where(['a.meal_id'=>$id])
                ->where('a.status','neq',2)
                ->field(['a.*','b.user_nickname'])
                ->order('a.id desc')
                ->select()->toArray();

            foreach ($list as &$val){


                $val['game_name']=$count['name'];
                $val['endtime']=date('y-m-d',$val['endtime']);


            }


            echo json_encode(['code'=>0,'msg'=>'','count'=>'10','data'=>$list]);
            die;
        }




        $id=$this->request->param('id');
        $this->assign("id", $id);
        return $this->fetch();
    }


    public function edit(){
        $id  = $this->request->param('id');

        $res = Db::name('meal')->where(['id'=>$id])->find();


        $this->assign('info',$res);
        return $this->fetch();

    }

    /*
     * 删除
     * */
    public function delete(){

        $id  = $this->request->post('id');

        $res = Db::name('meal')->where(['id'=>$id])->delete();

        if ($res){
            $this->success('操作成功!');

        }else{
            $this->error('删除成功');
        }


    }

    public function addPost(){

        $arrData  = $this->request->post('post');

        if (isset($arrData['id'])&&$arrData['id']>0){

            $id=$arrData['id'];
            unset($arrData['id']);
            Db::name('meal')->where(['id'=>$id])->update($arrData);

            $this->success("操作成功！",'mealindex/index');
        }else{
            if ($arrData['endtime']){

                $arrData['endtime']=strtotime($arrData['endtime']);
            }

            Db::name('meal')->data($arrData)->insert();

            $this->success("操作成功！",'mealindex/index');


        }




    }
    
    /*
     * 赠送
     * */


    //提交提交用户 uid,stype

    public function submitsmeal(){

        $mobile=$this->request->param('mobile');
        $mid=$this->request->param('id');



        $user=Db::name('user')->where(['id'=>$mobile])->find();

        $meal=Db::name('meal')->where(['id'=>$mid])->find();


        if ($user['score']<$meal['integrate']){

            return json(['code'=>0,'msg'=>'用户积分不足!']);

        }

        if ($meal['time_data']==1){
            $log['starttime']=time();
            $log['endtime']=strtotime('+'.$meal['day']. ' day');
        }else{
            $log['starttime']=$meal['starttime'];
            $log['endtime']=$meal['endtime'];
        }

        $log['user_id']=$user['id'];
        $log['meal_id']=$meal['id'];
        $log['score']=$meal['integrate'];
        $log['stype']=1;

        Db::name('meal_log')->data($log)->insert();


        $logid=scoresetdec($user['id'],$meal['integrate'],'兑换餐券');

        return json(['code'=>1,'msg'=>'操作成功!','logid'=>$logid]);


    }



}