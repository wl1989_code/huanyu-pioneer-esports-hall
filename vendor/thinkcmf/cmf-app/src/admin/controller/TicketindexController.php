<?php
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;
use app\admin\model\AdminMenuModel;
use wxapp\aes\ErrorCode;

class TicketindexController extends AdminBaseController
{

    public function index()
    {
        if ($this->request->isPost()){

            $limit=$this->request->param('limit');
            $page=$this->request->param('page');
            $limits=($page-1)*$limit .  ','. $limit;

            $map[]=['id','gt','0'];

            $status=$this->request->param('status');
            $name=$this->request->param('name');




            if ($status>0){
                $map[]=['status','eq',$status-1];
            }

             if ($name){
                $map[]=['name','like','%'.$name.'%'];
            }



            $list=Db::name('ticket')
                ->where($map)
                ->limit($limits)
                ->order('id desc')
                ->select()->toArray();

            foreach ($list as &$val){

                $count=Db::name('ticket_log')
                    ->where(['ticket_id'=>$val['id']])
                    ->count();
                $val['inuser']=$count;

                $counts=Db::name('ticket_log_l')
                    ->where(['ticket_id'=>$val['id']])

                    ->count();

                $val['zs']=$counts;

            }
            $counts=Db::name('ticket')
                ->where($map)->count();

            echo json_encode(['code'=>0,'msg'=>'','count'=>$counts,'data'=>$list]);
            die;

        }


        return $this->fetch();

    }


    //删除

    public  function delete(){

        $id  = $this->request->post('id');


        Db::name('ticket')->where(['id'=>$id])->delete();

        $this->success("操作成功！");
    }

    //添加信息
    public function add(){
        return $this->fetch();
    }

    public function edit(){

        $id  = $this->request->param('id');
        $info=Db::name('ticket')->where(['id'=>$id])->find();

        if ($info['time_data']==2){
            $info['starttime']=date('Y-m-d H:i:s',$info['starttime']);
            $info['endtime']=date('Y-m-d H:i:s',$info['endtime']);
        }else{
            $info['starttime']='';
            $info['endtime']='';
        }



        $this->assign("info", $info);

        return $this->fetch();
    }

    //添加修改数据

    public function addPost(){


        $arrData  = $this->request->post('post');

        $arrData['starttime']=strtotime($arrData['starttime']);
        $arrData['endtime']=strtotime($arrData['endtime']);

        if (isset($arrData['id'])&&$arrData['id']>0){

            $id=$arrData['id'];
            unset($arrData['id']);
            Db::name('ticket')->where(['id'=>$id])->update($arrData);

            $this->success("操作成功！",'ticketindex/index');
        }else{
            Db::name('ticket')->data($arrData)->insert();

            $this->success("操作成功！",'ticketindex/index');

        }

    }


    /**
     * 赠送
      */
    public function zengsong(){


        if ($this->request->isPost()){

            $uid = $this->request->param('uid');
            $tid  = $this->request->param('tid');
            $number = $this->request->param('number');

            if ($number<1){
                $this->error('请填写数量');
            }

            $userinfo=Db::name('user')->where(['id'=>$uid])->find();

            if (!$userinfo){
                $this->error('没有找到用户');
            }

            $game=Db::name('ticket')->where(['id'=>$tid])->find();

            if (!$game){
                $this->error('没有比赛信息');
            }


            //查询是否有相同的全  持有
            $have['user_id']=$userinfo['id'];
            $have['ticket_id']=$tid;

            $have_i=Db::name('ticket_log')->where($have)->find();

            if ($have_i){

                $log1['user_id']=$userinfo['id'];
                $log1['ticket_id']=$tid;
                $numbers=$number+$have_i['count'];

                Db::name('ticket_log')->where($log1)->update(['count'=>$numbers]);

            }else{

                $logs['user_id']=$userinfo['id'];
                $logs['ticket_id']=$tid;
                $logs['starttime']=time();
                $logs['endtime']=strtotime('+100 day');
                $logs['count']=$number;
                $logs['score']=0;
                $logs['get']=2;
                $logs['gettime']=time();

                Db::name('ticket_log')->insertGetId($logs);

            }


            $logb['user_id']=$userinfo['id'];
            $logb['ticket_id']=$tid;
            $logb['f_user']=1;
            $logb['count']=$number;
            $logb['score']=0;
            $logb['get']=1;
            $logb['desc']='后台赠送券';
            $logb['gettime']=time();

            $logid= Db::name('ticket_log_l')->insertGetId($logb);

            $this->success('操作成功!','',['logid'=>$logid]);


        }

        $id=$this->request->param('id');

        $this->assign('id',$id);

        return $this->fetch();
    }

    /*
        * 兑换
        * */
    public function duihuan(){
        if ($this->request->isPost()){

            $uid = $this->request->param('uid');
            $tid  = $this->request->param('tid');
            $number = $this->request->param('number');

            if ($number<1){
                $this->error('请输入数量');
            }

            $userinfo=Db::name('user')->where(['id'=>$uid])->find();

            if (!$userinfo){
                $this->error('没有找到用户'.$uid);
            }

            $game=Db::name('ticket')->where(['id'=>$tid])->find();


            $endint=$number*$game['integrate'];

            if ($userinfo['score']<$endint){
                $this->error('用户积分不足');
            }



            //查询是否有相同的全  持有
            $have['user_id']=$userinfo['id'];
            $have['ticket_id']=$tid;

            $have_i=Db::name('ticket_log')->where($have)->find();

            if ($have_i){

                $log1['user_id']=$userinfo['id'];
                $log1['ticket_id']=$tid;
                $numbers=$number+$have_i['count'];

                Db::name('ticket_log')->where($log1)->update(['count'=>$numbers]);

            }else{

                $logs['user_id']=$userinfo['id'];
                $logs['ticket_id']=$tid;
                $logs['starttime']=time();
                $logs['endtime']=strtotime('+100 day');
                $logs['count']=$number;
                $logs['score']=0;
                $logs['gettime']=time();

                Db::name('ticket_log')->insertGetId($logs);

            }


            $logb['user_id']=$userinfo['id'];
            $logb['ticket_id']=$tid;
            $logb['f_user']=1;
            $logb['count']=$number;
            $logb['score']=$endint;
            $logb['get']=3;
            $logb['desc']='后台积分兑换';
            $logb['gettime']=time();

            $logid= Db::name('ticket_log_l')->insertGetId($logb);
            scoresetdec($userinfo['id'],$endint,'兑换扣除');

            $this->success('操作成功','',['logid'=>$logid]);
        }

        $id=$this->request->param('id');
        $this->assign('id',$id);
        return $this->fetch();


    }

    // 列表

    public function inuser(){


        if ($this->request->isPost()){

            $id=$this->request->param('id');
            $name=$this->request->param('name');

            $status=$this->request->param('status','-1');

            $count=Db::name('ticket')->where(['id'=>$id])->find();

            $list=Db::name('ticket_log')
                ->alias('a')
                ->join('cmf_user b','a.user_id=b.id')
                ->where(['a.ticket_id'=>$id])
                ->where('b.user_nickname','like','%'.$name.'%')
                ->field(['a.*','b.user_nickname'])
                ->order('a.id desc')
                ->select()->toArray();

            foreach ($list as &$val){
                $val['gettime']=$val['gettime']?date('Y-m-d H:i',$val['gettime']):'';
                $val['game_name']=$count['name'];
            }


            echo json_encode(['code'=>0,'msg'=>'','count'=>'10','data'=>$list]);
            die;
        }




        $id=$this->request->param('id');
        $this->assign("id", $id);
        return $this->fetch();

    }


    public function deletelog(){

        $id  = $this->request->post('id');


        Db::name('ticket_log')->where(['id'=>$id])->delete();

        $this->success("操作成功！");



    }

    public function userzs()
    {

        $id=$this->request->param('id');

        if ($this->request->isPost()) {

            $count = Db::name('ticket')->where(['id' => $id])->find();

            $uid=$this->request->param('name');
            $start_time=$this->request->param('start_time');
            $end_time=$this->request->param('end_time');



            $map['a.ticket_id']=['eq',$id];

            if ($uid){
                $map['a.user_id']=['eq',$uid];
            }

            if ($start_time){
                $map['a.gettime']=['gt',strtotime($start_time)];
            }

            if ($end_time){
                $map['a.gettime']=['lt',strtotime($end_time)];
            }

            $list = Db::name('ticket_log_l')
                ->alias('a')
                ->join('cmf_user b', 'a.user_id=b.id')
                ->join('cmf_user c', 'a.f_user=c.id')
//                ->where(['a.ticket_id' => $id])
//                ->where('a.get', 'eq', 1)
//                ->where('c.user_nickname', 'like', '%'.$name.'%')
                ->where($map)
                ->field(['a.*', 'b.user_nickname','c.user_nickname fu_name'])
                ->order('a.gettime desc')
                ->select()->toArray();


            foreach ($list as &$val) {

                $val['gettime'] = $val['gettime'] ? date('Y-m-d H:i', $val['gettime']) : '';
                $val['game_name'] = $count['name'];

            }


            echo json_encode(['code' => 0, 'msg' => '', 'count' =>count($list), 'data' => $list]);
            die;
        }
        $id=$this->request->param('id');
        $this->assign("id", $id);
        return $this->fetch();



        }

}


