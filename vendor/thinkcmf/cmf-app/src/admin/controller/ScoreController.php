<?php

namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;
use app\admin\model\AdminMenuModel;
use wxapp\aes\ErrorCode;

class ScoreController extends AdminBaseController
{

    public function index()
    {
        if ($this->request->isPost()) {

            $limit = $this->request->param('limit');
            $page = $this->request->param('page');
            $limits = ($page - 1) * $limit . ',' . $limit;

            $map[] = ['a.id', 'gt', 0];

            $status = $this->request->param('status','-1');

            $name = $this->request->param('name');
            if ($name) {
                $map[] = ['b.user_nickname', 'like', '%' . $name . '%'];
            }


            $starttime = $this->request->param('starttime');
            if ($starttime) {
                $map[] = ['a.create_time', 'gt', strtotime($starttime)];
            }

            $endtime = $this->request->param('endtime');
            if ($endtime) {
                $map[] = ['a.create_time', 'lt', strtotime($endtime)];
            }



            $get1=   Db::name('user_score_log')
                ->alias('a')
                ->join('user b','a.user_id=b.id')
                ->where('a.score','gt',0)
                ->where($map)
                ->sum('a.score');

            $get2=   Db::name('user_score_log')
                ->alias('a')
                ->join('user b','a.user_id=b.id')
                ->where($map)
                ->where('a.action','eq','后台充值')
                ->sum('a.score');

            $get3=   Db::name('user_score_log')
                ->alias('a')
                ->where($map)
                ->join('user b','a.user_id=b.id')
                ->where('a.action','eq','后台赠送')
                ->sum('a.score');

            $get4=   Db::name('user')
                ->where('user_nickname','like','%'.$name.'%')

                ->where('user_type','eq',2)
                ->sum('score');




            if ($status == 0) {
                $map[] = ['a.score', 'lt', 0];
            }else if ($status == 1) {
                $map[] = ['a.score', 'gt', 0];
            }else if ($status == 2) {
                $map[] = ['a.action', 'eq', '后台充值'];
            }else if ($status == 3) {
                $map[] = ['a.action', 'eq', '后台赠送'];
            }



            $list=Db::name('user_score_log')
                ->alias('a')
                ->join('cmf_user b','a.user_id=b.id')
                ->where($map)
                ->field(['a.*','b.user_nickname','b.mobile'])
                ->order('a.id desc')
                ->limit($limits)
                ->select()->toArray();

//            $ret=Db::getLastSql();
//            echo $ret;

            foreach ($list as &$val){
                $val['create_time']=date('y-m-d H:i',$val['create_time']);
            }


            $count=   Db::name('user_score_log')
                ->alias('a')
                ->join('cmf_user b','a.user_id=b.id')
                ->where($map)
                ->count();





            $str='总积分:'.$get4 .' &nbsp; ' .' 充值积分:'.$get2.' 赠送积分:'.$get3;

            echo json_encode(['code' => 0, 'msg' =>$str, 'count' => $count, 'data' => $list]);
            die;

        }

        //12
        return $this->fetch();

    }

}