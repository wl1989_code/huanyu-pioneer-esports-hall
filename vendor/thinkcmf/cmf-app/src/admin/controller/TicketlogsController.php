<?php

namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;
use app\admin\model\AdminMenuModel;
use wxapp\aes\ErrorCode;

class TicketlogsController extends AdminBaseController
{

    public function index()
    {
        if ($this->request->isPost()) {

            $limit = $this->request->param('limit');
            $page = $this->request->param('page');
            $limits = ($page - 1) * $limit . ',' . $limit;

            $map[] = ['a.id', 'gt', 0];

            $status = $this->request->param('status','-1');
            $name = $this->request->param('tname');
            $user = $this->request->param('uname');

            if ($name) {
                $map[] = ['c.name', 'like', '%' . $name . '%'];
            }

            if ($user) {
                $map[] = ['b.user_nickname', 'like', '%' . $user . '%'];
            }

            $list=Db::name('ticket_log')
                ->alias('a')
                ->join('cmf_user b','a.user_id=b.id')
                ->join('cmf_ticket c','c.id=a.ticket_id')
                ->where($map)
                ->field(['a.*','b.user_nickname','c.name'])
                ->order('a.id desc')
                ->limit($limits)
                ->select()->toArray();


            foreach ($list  as &$key){
                $key['gettime']=$key['gettime']?date('Y-m-d H:i',$key['gettime']):'';
                if ($key['f_user']){
                    $fuser=Db::name('user')->where(['id'=>$key['f_user']])->find();
                    $key['fuser']=$fuser['user_nickname'];
                }else{
                    $key['fuser']='';
                }
            }

            $count=   Db::name('ticket_log')
                ->alias('a')
                ->join('cmf_user b','a.user_id=b.id')
                ->join('cmf_ticket c','c.id=a.ticket_id')
                ->where($map)
                ->sum('a.count');

            $counts=   Db::name('ticket_log')
                ->alias('a')
                ->join('cmf_user b','a.user_id=b.id')
                ->join('cmf_ticket c','c.id=a.ticket_id')
                ->where($map)
                ->count();


            echo json_encode(['code' => 0, 'msg' =>$count, 'count' => $counts, 'data' => $list]);
            die;

        }


        return $this->fetch();

    }

    public function delete(){

        $id  = $this->request->post('id');


        Db::name('ticket_log')->where(['id'=>$id])->delete();

        $this->success("操作成功！");



    }


    public function seelogs(){


        $id  = $this->request->param('id');


        if ($this->request->isPost()) {

            $log=Db::name('ticket_log')->where(['id'=>$id])->find();


            $limit = $this->request->param('limit');
            $page = $this->request->param('page');
            $limits = ($page - 1) * $limit . ',' . $limit;

            $map[] = ['a.user_id', 'eq', $log['user_id']];
            $map[] = ['a.ticket_id', 'eq', $log['ticket_id']];

            $status = $this->request->param('status','-1');

            if ($status>0) {
                $map[] = ['a.status', 'eq', $status];
            }

            $gets= $this->request->param('gets');
            if ($gets>0) {
                $map[] = ['a.get', 'eq', $gets];
            }


            $start_time= $this->request->param('start_time');
            if ($start_time) {
                $map[] = ['a.gettime', 'gt', strtotime($start_time)];
            }

            $end_time= $this->request->param('end_time');
            if ($end_time) {
                $map[] = ['a.gettime', 'lt', strtotime($end_time)];
            }

            $list=Db::name('ticket_log_l')
                ->alias('a')
                ->join('cmf_user b','a.user_id=b.id')
                ->join('cmf_ticket c','c.id=a.ticket_id')
                ->where($map)
                ->field(['a.*','b.user_nickname','c.name'])
                ->order('a.id desc')
                ->limit($limits)
                ->select()->toArray();


            foreach ($list  as &$key){

                $key['gettime']=$key['gettime']?date('Y-m-d H:i',$key['gettime']):'';

                if ($key['f_user']){
                    $fuser=Db::name('user')->where(['id'=>$key['f_user']])->find();
                    $key['fuser']=$fuser['user_nickname'];
                }else{
                    $key['fuser']='';
                }
            }

            $count=   Db::name('ticket_log')
                ->alias('a')
                ->join('cmf_user b','a.user_id=b.id')
                ->join('cmf_ticket c','c.id=a.ticket_id')
                ->where($map)
                ->sum('a.count');

            $counts=   Db::name('ticket_log')
                ->alias('a')
                ->join('cmf_user b','a.user_id=b.id')
                ->join('cmf_ticket c','c.id=a.ticket_id')
                ->where($map)
                ->count();


            echo json_encode(['code' => 0, 'msg' =>$count, 'count' => $counts, 'data' => $list]);
            die;

        }



        $this->assign('id',$id);
        return $this->fetch();

    }

}