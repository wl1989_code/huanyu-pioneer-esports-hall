<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2017 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: Dean <zxxjjforever@163.com>
// +----------------------------------------------------------------------
namespace api\wxapp\controller;

use cmf\controller\RestBaseController;
use think\Db;
use wxapp\aes\WXBizDataCrypt;

class UserController extends RestBaseController
{
    // 获取用户信息
    public function getUserInfo()
    {

        $uid=$this->getUserId();

        $user=Db::name('user')->where(['id'=>$uid])->find();

        $this->success('获取用户信息', $user);

    }



    public function getscoredec(){

        $uid=$this->getUserId();

        $user=Db::name('user')->where(['id'=>$uid])->find();

        $list=Db::name('user_score_log')
            ->where(['user_id'=>$user['id']])
            ->where('score','lt',0)
            ->select()->toArray();

        foreach ($list as &$key){
            $key['createtime']=date('y-m-d H:i',$key['create_time']);

        }

        $this->success('操作成功',$list);
    }

    public function getscoreinc(){

        $uid=$this->getUserId();

        $user=Db::name('user')->where(['id'=>$uid])->find();

        $list=Db::name('user_score_log')
            ->where(['user_id'=>$user['id']])
            ->where('score','gt',0)
            ->select()->toArray();

        foreach ($list as &$key){
            $key['createtime']=date('y-m-d H:i',$key['create_time']);

        }


        $this->success('操作成功',$list);
    }

}
