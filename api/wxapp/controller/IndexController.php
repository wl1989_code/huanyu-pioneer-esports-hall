<?php




namespace api\wxapp\controller;
use cmf\controller\RestUserBaseController;
use think\Db;


class IndexController extends RestUserBaseController
{


    /*
     * 首页幻灯片
     * */
    public function getbanner()
    {
        $list = Db::name('banner')->where(['status' => 1])->select()->toArray();
        foreach ($list as &$key) {
            $key['image'] = cmf_get_image_url($key['image']);
        }
        $this->success('幻灯片', $list);
    }



    /*
    * 首页赛事
    * */
    public function getindexgame(){


        $list=Db::name('game')->where(['top'=>1,'status'=>1])->order('id desc')->limit(5)->select();

        $new=[];
        foreach ($list as $v){

            $v['image']=cmf_get_image_url($v['image']);
            $v['starttime']=date('Y-m-d H:i',$v['starttime']?$v['starttime']:strtotime("+30 day"));
            $new[]=$v;
        }
        $this->success('首页赛事1',$new);
    }



    /*
     * 首页券
     * */

    public function getindexticket(){

        $list=Db::name('ticket')->where(['top'=>1,'status'=>1])->order('id desc')->limit(5)->select();

        $new=[];
        foreach ($list as &$key){
            $key['endtime']=date('Y-m-d H:i',$key['endtime']);
            $key['starttime']=date('Y-m-d H:i',$key['starttime']?$key['starttime']:strtotime("+30 day"));

            $new[]=$key;
        }
        $this->success('首页券',$new);

    }


    public function getallticket(){

        $list=Db::name('ticket')->where(['status'=>1])->order('id desc')->limit(5)->select();

        $new=[];
        foreach ($list as &$key){
            $key['endtime']=date('Y-m-d H:i',$key['endtime']);
            $key['starttime']=date('Y-m-d H:i',$key['starttime']?$key['starttime']:date(strtotime('+30 day')));
            $new[]=$key;
        }
        $this->success('首页券',$new);
    }

    public function getallgame(){

//        $list=Db::name('game')->where(['status'=>1])->order('id desc')->limit(5)->select();

        $new=[];
//        foreach ($list as &$key){
//            $key['endtime']=date('Y-m-d H:i',$key['endtime']);
//            $key['starttime']=date('Y-m-d H:i',$key['starttime']);
//            $key['image']=cmf_get_image_url($key['image']);
//            $new[]=$key;
//
//        }
        $this->success('全部比赛',$new);

    }


    public function getgamebyid(){

        $id=$this->request->param('id');

        $key=Db::name('game')->where(['id'=>$id])->find();

        $key['endtime']=date('Y-m-d H:i',$key['endtime']);
        $key['starttime']=date('Y-m-d H:i',$key['starttime']);
        $key['image']=cmf_get_image_url($key['image']);


        $counts=Db::name('game_log')->count();
        $key['in']=$counts;


        $this->success('比赛',$key);


    }



}