<?php

namespace api\wxapp\controller;
use cmf\controller\RestUserBaseController;
use think\Db;


class GameController extends RestUserBaseController
{


    /*
     *用户兑换参数
     * */


    /**
     * @return bool
     */
    public function exchangegame(){


        $uid=$this->getUserId();
        $userinfo=Db::name('user')->where(['id'=>$uid])->find();

        $id=$this->request->param('id');
        $number=$this->request->param('number');
        $number=intval($number);


        if ($number<1){
            $this->error('兑换数量错误');
        }

        $user=Db::name('user')->where(['id'=>$userinfo['id']])->find();


        $game=Db::name('ticket')->where(['id'=>$id])->find();


        $jifen=$game['integrate']*$number;


        if ($jifen>$user['score']){
            $this->error('积分不足!');
        }


        //如果有记录 则添加记录

        $logs['user_id']=$user['id'];
        $logs['ticket_id']=$id;

        $have=Db::name('ticket_log')
            ->where($logs)
            ->find();

        var_dump($have);
        if ($have){

            $a=$have['count']+$number;

            Db::name('ticket_log')
                ->where($logs)
                ->update(['count'=>$a]);

        }else{

            $logs['count']=$number;
            $logs['gettime']=time();
            Db::name('ticket_log')
                ->data($logs)
                ->insert();

        }


        scoresetdec($user['id'],$jifen,'兑换参赛券');



        $logc['user_id']=$user['id'];
        $logc['ticket_id']=$id;
        $logc['f_user']=1;
        $logc['count']=$number;
        $logc['get']=3;
        $logc['status']=1;
        $logc['desc']='用户兑换';
        $logc['gettime']=time();

        Db::name('ticket_log_l')->data($logc)->insert();


        $this->success('兑换成功');
    }


    //我的参赛记录

    public function getmytackbytype(){

        $uid=$this->getUserId();
        $userinfo=Db::name('user')->where(['id'=>$uid])->find();


        $status=$this->request->param('status');

        $map['user_id']=$userinfo['id'];

        if ($status==0){
            $map['status']=0;
            $lists=Db::name('ticket_log')->where($map)->order('id desc')->select()->toArray();

        }

        foreach ($lists as &$key){

            $game=Db::name('ticket')->where(['id'=>$key['ticket_id']])->find();
            $key['name']=$game['name'];
            $key['starttime']=date('Y-m-d',$key['starttime']);
            $key['endtime']=date('Y-m-d',$key['endtime']);
        }

        $this->success('我的参赛记录',$lists);

    }


    public function getmytackbytype1(){

        $uid=$this->getUserId();
        $userinfo=Db::name('user')->where(['id'=>$uid])->find();


        $map['user_id']=$userinfo['id'];
        $map['status']=1;

        $lists=Db::name('ticket_log_l')
            ->where($map)
            ->order('id desc')->select()->toArray();


        foreach ($lists as &$key){

            $game=Db::name('ticket')->where(['id'=>$key['ticket_id']])->find();
            $key['name']=$game['name'];
            $key['starttime']=date('Y-m-d',$key['gettime']);

        }

        $this->success('我的参赛记录',$lists);

    }



    public function getmytackbytype2(){

        $uid=$this->getUserId();
        $userinfo=Db::name('user')->where(['id'=>$uid])->find();


        $map['user_id']=$userinfo['id'];
        $map['status']=2;
        $map['desc']='赠送用户';

        $lists=Db::name('ticket_log_l')
            ->where($map)
            ->order('id desc')->select()->toArray();


        foreach ($lists as &$key){

            $game=Db::name('ticket')->where(['id'=>$key['ticket_id']])->find();
            $key['name']=$game['name'];

            $users=Db::name('user')->where(['id'=>$key['f_user']])->find();

            $key['fname']=$users['user_nickname'];

            $key['starttime']=date('Y-m-d',$key['gettime']);

        }

        $this->success('我的参赛记录',$lists);

    }




    public function getmytackbytype3(){

        $uid=$this->getUserId();
        $userinfo=Db::name('user')->where(['id'=>$uid])->find();


        $map['user_id']=$userinfo['id'];
        $map['status']=2;
        $map['get']=3;

        $lists=Db::name('ticket_log_l')
            ->where($map)
            ->order('id desc')->select()->toArray();


        foreach ($lists as &$key){

            $game=Db::name('ticket')->where(['id'=>$key['ticket_id']])->find();
            $key['name']=$game['name'];

            $users=Db::name('user')->where(['id'=>$key['f_user']])->find();

            $key['fname']=$users['user_nickname'];

            $key['starttime']=date('Y-m-d',$key['gettime']);

        }

        $this->success('我的参赛记录',$lists);

    }




    public function getinfobyid(){

        $uid=$this->getUserId();
        $userinfo=Db::name('user')->where(['id'=>$uid])->find();


        $id=$this->request->param('id');

        $lists=Db::name('ticket_log')
            ->where('status','eq',0)
            ->where(['id'=>$id])->where(['user_id'=>$userinfo['id']])
            ->find();


        if (!$lists){
            $this->error('无可用券');
        }

        $game=Db::name('ticket')->where(['id'=>$lists['ticket_id']])->find();
        $lists['name']=$game['name'];
        $lists['starttime']=date('y-m-d',$lists['starttime']);
        $lists['endtime']=date('y-m-d',$lists['endtime']);


        $this->success('参赛记录详情',$lists);

    }


    // 赠送卡券

    public function givetick(){


        $muid=$this->getUserId();
        $userinfo=Db::name('user')->where(['id'=>$muid])->find();


        $id=$this->request->param('id'); //券id
        $uid=$this->request->param('uid'); //赠送对象

        $allcount=$this->request->param('scount'); //数量



        $list=Db::name('ticket_log')
            ->where(['id'=>$id])->where(['user_id'=>$userinfo['id']])
            ->find();

        if (!$list){
            $this->error('无可用参数券');
        }

        if ($list['count']<$allcount){
            $this->error('参数券不足');
        }

        $giveuser=Db::name('user')->where(['id'=>$uid])->find();

        if (!$giveuser){
            $this->error('没有找到赠送对象');
        }

        if ($uid==$userinfo['id']){
            $this->error('请不要对自己使用!');
        }

            $hostnum=$list['count']-$allcount;

            Db::name('ticket_log')
                ->where('id','eq',$list['id'])
                ->update(['count'=>$hostnum]);


            $f_user=Db::name('ticket_log')->where(['user_id'=>$giveuser['id'],'ticket_id'=>$list['ticket_id']])->find();

            if ($f_user){

                Db::name('ticket_log')->where(['user_id'=>$giveuser['id'],'ticket_id'=>$list['ticket_id']])
                    ->setInc('count',$allcount);
            }else{

                $sends1['user_id']=$giveuser['id'];
                $sends1['ticket_id']=$list['ticket_id'];
                $sends1['gettime']=time();
                $sends1['count']=$allcount;

                Db::name('ticket_log')
                    ->data($sends1)
                    ->insert();
            }


        $logc['user_id']=$giveuser['id'];
        $logc['ticket_id']=$list['ticket_id'];
        $logc['f_user']=$userinfo['id'];
        $logc['count']=$allcount;
        $logc['get']=2;
        $logc['status']=1;
        $logc['desc']='用户赠送';
        $logc['gettime']=time();

        Db::name('ticket_log_l')->data($logc)->insert();


        $logd['user_id']=$userinfo['id'];
        $logd['ticket_id']=$list['ticket_id'];
        $logd['f_user']= $giveuser['id'];
        $logd['count']=$allcount;
        $logd['get']=2;
        $logd['status']=2;
        $logd['desc']='赠送用户';
        $logd['gettime']=time();

        Db::name('ticket_log_l')->data($logd)->insert();



        $this->success('操作成功!');

    }


    /**
     * @return bool
     */
    public function getmymeallog()
    {
        $status=$this->request->param('status');

        $uid=$this->getUserId();
        $user=Db::name('user')->where(['id'=>$uid])->find();

        Db::name('meal_log')->where('endtime','lt',time())->update(['status'=>2]);



        $map['user_id']=$user['id'];
        $map['status']=$status;

        $list=Db::name('meal_log')->where($map)->order('id desc')->select()->toArray();

        foreach ($list as &$key){

            $meal=Db::name('meal')->where(['id'=>$key['meal_id']])->find();

            $key['starttime']=date('y-m-d ',$key['starttime']);
            $key['endtime']=date('y-m-d ',$key['endtime']);
            $key['mname']=$meal['name'];

        }

        $this->success('我的餐券!',$list);

    }



}

