<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2017 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: pl125 <xskjs888@163.com>
// +----------------------------------------------------------------------
namespace api\portal\controller;

use api\portal\model\PortalPostModel;
use cmf\controller\RestBaseController;
use api\portal\model\PortalTagModel;

class IndexController extends RestBaseController
{
    protected $tagModel;

    /**
     * 获取标签列表
     */
    public function test()
    {
        $this->success('请求成功!', "DD");
    }



    /**
     *Title:支付回调
     *<br/>By: 王龙 2022/8/29 17:23
     */

    public function paybackc(){

        $input=file_get_contents("php://input");

        $obj = simplexml_load_string($input, 'SimpleXMLElement', LIBXML_NOCDATA);

        $data = json_decode(json_encode($obj), true);



        Db::name('pay_log')->where(['orderid'=>$data['out_trade_no']])->where(['status'=>0])->find();



    }


    /**
     *Title:文件上传
     *<br/>By: 王龙 2022/8/29 17:23
     */
    public function one(){

        $file = request()->file('file');


        $info = $file->move( './upload');
        $url=$info->getFilename();

        $this->success("恭喜您,API访问成功!", ['img'=>'http://'.$_SERVER['HTTP_HOST'].'/upload/'.date('Ymd').'/'.$url]);
    }

}
